mod seat;
mod plane;

use seat::Seat;
use plane::Plane;
use seat::Rating::{Empty, Terrible, Bad, Okay, Good, Awesome};

fn main() {
    let _a_seat = Seat::new(1, 1).with_shoulder(Good).with_leg(Awesome);
    let _a_seat = Seat::new(1, 1).with_shoulder(Empty).with_leg(Terrible);
    let a_seat = Seat::new(1, 1).with_shoulder(Bad).with_leg(Okay);
    println!("Seat: {:?}", a_seat);

    let a_plane = Plane::new("no make", "no model", 1, 1);
    println!("{:?}", a_plane);
}

