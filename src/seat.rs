#[derive(Debug)]
pub struct Seat {
    row: i8,
    column: i8,
    shoulder: Rating,
    leg: Rating
}

#[derive(Debug)]
#[derive(PartialEq)]
pub enum Rating {
    Empty,
    Terrible,
    Bad,
    Okay,
    Good,
    Awesome
}

impl Seat {
    pub fn new(row: i8, column: i8) -> Seat {
        Seat { row, column, shoulder: Rating::Empty, leg: Rating::Empty}
    }

    pub fn with_shoulder(self, description: Rating) -> Seat {
        Seat {
            shoulder: description,
            .. self
        }
    }

    pub fn with_leg(self, leg: Rating) -> Seat {
        Seat {
            leg: leg,
            .. self
        }
    }
}

#[cfg(test)]
mod test_seat {
    use super::*;

    #[test]
    fn new_seat_creats_correct_seat() {
        let seat = Seat::new(1, 1);

        assert_eq!(seat.row, 1);
        assert_eq!(seat.column, 1);
    }

    #[test]
    fn shoulder_changes() {
        let seat = Seat::new(1, 1);

        assert_eq!(seat.row, 1);
        assert_eq!(seat.column, 1);

        let seat = seat.with_shoulder(Rating::Bad);
        assert_eq!(seat.shoulder, Rating::Bad);

        let seat = seat.with_shoulder(Rating::Terrible);
        assert_eq!(seat.shoulder, Rating::Terrible);
    }

    #[test]
    fn leg_changes() {
        let seat = Seat::new(1, 1);

        assert_eq!(seat.row, 1);
        assert_eq!(seat.column, 1);

        let seat = seat.with_leg(Rating::Bad);
        assert_eq!(seat.leg, Rating::Bad);

        let seat = seat.with_leg(Rating::Okay);
        assert_eq!(seat.leg, Rating::Okay);
    }
}
