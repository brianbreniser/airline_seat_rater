use crate::seat::Seat;

#[derive(Debug)]
pub struct Plane {
    make: &'static str,
    model: &'static str,
    seats: Vec<Seat>,
}

impl Plane {
    pub fn new(make: &'static str, model: &'static str, num_rows: i8, num_columns: i8) -> Plane {
        let mut seats = Vec::new();

        for row in 1..num_rows+1 { // rows start at 1
            for col in 1..num_columns+1 { // columns start at 1
                seats.push(Seat::new(row, col));
            }
        }

        Plane {
            make,
            model,
            seats
        }
    }
}


#[cfg(test)]
mod test_plane {
    use super::*;

    #[test]
    fn new_plane_creats_correct_number_of_seats() {
        let plane = Plane::new("no make", "no model", 1, 1);
        assert_eq!(1, plane.seats.len());

        let plane = Plane::new("no make", "no model", 1, 2);
        assert_eq!(2, plane.seats.len());

        let plane = Plane::new("no make", "no model", 2, 1);
        assert_eq!(2, plane.seats.len());

        let plane = Plane::new("no make", "no model", 2, 2);
        assert_eq!(4, plane.seats.len());

        let plane = Plane::new("no make", "no model", 5, 100);
        assert_eq!(500, plane.seats.len());
    }
}
